import json, pika, django, os, sys, time
from django.core.mail import send_mail
from pika.exceptions import AMQPConnectionError

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def main():
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host="rabbitmq")
    )
    channel = connection.channel()
    channel.queue_declare(queue="presentation_approvals")
    channel.queue_declare(queue="presentation_rejections")

    def callback_approvals(ch, method, properties, body):
        print("[x] Received approval, email sent")
        content = json.loads(body)
        send_mail(
            f'Your presentation for {content["conference_name"]} has been approved',
            f"{content['presenter_name']}, we're happy to tell you that your presentation {content['title']} has been accepted",
            "admin@conference.go",
            [content["presenter_email"]],
            fail_silently=False,
        )

    def callback_rejections(ch, method, properties, body):
        print("[x] Received rejections, email sent")
        content = json.loads(body)
        send_mail(
            f'Your presentation for {content["conference_name"]} has been rejected',
            f"{content['presenter_name']}, Unfortuntately your presentation {content['title']} has been rejected",
            "admin@conference.go",
            [content["presenter_email"]],
            fail_silently=False,
        )

    channel.basic_consume(
        queue="presentation_rejections",
        on_message_callback=callback_rejections,
        auto_ack=True,
    )

    channel.basic_consume(
        queue="presentation_approvals",
        on_message_callback=callback_approvals,
        auto_ack=True,
    )

    channel.start_consuming()


while True:
    try:
        main()
    except KeyboardInterrupt:
        print("Interrupted")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
