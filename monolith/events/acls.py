from .keys import PEXELS_API_KEY, OPEN_WEATHER_APP_KEY
import requests


def get_photo(city, state):

    query = f"query={city}+{state}&per_page=1"
    url = f"https://api.pexels.com/v1/search?{query}"

    header = {"Authorization": PEXELS_API_KEY}

    response = requests.get(url, headers=header)
    data = response.json()
    photo_url = data["photos"][0]["src"]["original"]

    return photo_url


def get_weather(city, state, country):
    url_for_coord = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},{country}&limit=1&appid=d44d5f4d19cfca53c71b7fd7af7fd2cd"

    response1 = requests.get(url_for_coord)
    data = response1.json()
    try:
        lat = data[0]["lat"]
        lon = data[0]["lon"]
    except (IndexError, KeyError):
        return None

    params = {
        "lat": lat,
        "lon": lon,
        "units": "imperial",
        "appid": OPEN_WEATHER_APP_KEY,
    }

    url = "https://api.openweathermap.org/data/2.5/weather?"
    response2 = requests.get(url, params=params)

    weather_data = response2.json()
    temp = weather_data["main"]["temp"]
    description = weather_data["weather"][0]["description"]

    return {
        "temp": f"{temp}F",
        "description": description,
    }
