from django.http import JsonResponse
from common.json import ModelEncoder
from .models import Conference, Location, State
import json
from django.views.decorators.http import require_http_methods
from .acls import get_photo, get_weather


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
    ]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
        "location",
    ]

    encoders = {"location": LocationListEncoder()}


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = ["name"]


###


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"message": "Invalid Location id"}, status=400)
        conference = Conference.objects.create(**content)
        return JsonResponse(conference, ConferenceDetailEncoder, safe=False)

    else:
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences}, ConferenceListEncoder, safe=False
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_conference(request, id):
    if request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse({"Message": "Invalid location id"}, status=400)
        Conference.objects.filter(id=id).update(**content)
        conference = Conference.objects.get(id=id)
        return JsonResponse(conference, ConferenceDetailEncoder, safe=False)

    elif request.method == "DELETE":
        count, _ = Conference.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        conference = Conference.objects.get(id=id)
        return JsonResponse(
            {
                "weather": get_weather(
                    conference.location.city,
                    conference.location.state.abbreviation,
                    'US'
                ),
                "conference": conference,
            },
            ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"}, status=400
            )
        location = Location.objects.create(**content)
        photo_url = get_photo(location.city, location.state.name)
        location.picture_url = photo_url
        location.save()
        return JsonResponse(location, LocationDetailEncoder, safe=False)

    else:
        locations = Location.objects.all()

        return JsonResponse(
            {"location": locations}, LocationListEncoder, safe=False
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_location(request, id):
    if request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"Message": "Invalid state abbreviation"}, status=400
            )

        Location.objects.filter(id=id).update(**content)

        location = Location.objects.get(id=id)
        return JsonResponse(location, LocationDetailEncoder, safe=False)

    elif request.method == "DELETE":
        count, _ = Location.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    else:
        location = Location.objects.get(id=id)

        if location.picture_url is None:
            photo_url = get_photo(location.city, location.state.name)
            location.picture_url = photo_url
            location.save()

        return JsonResponse(
            location,
            LocationDetailEncoder,
            safe=False,
        )
