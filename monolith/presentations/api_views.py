from django.http import JsonResponse
from common.json import ModelEncoder

# from attendees.api_views import ConferenceDetailEncoder
from django.views.decorators.http import require_http_methods
from .models import Presentation
from events.models import Conference
import json, pika


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
    ]


class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]

    def get_extra_data(self, o):
        return {"status": o.status.name}


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
        "conference",
    ]

    def get_extra_data(self, o):
        return {"status": o.status.name}

    encoders = {"conference": ConferenceDetailEncoder()}


#############################################################################


@require_http_methods(["GET", "POST"])
def api_list_presentations(request, conference_id):
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
            content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse({"message": "Invalid Conference ID"})

        presentation = Presentation.create(**content)
        return JsonResponse(
            presentation, PresentationDetailEncoder, safe=False
        )

    else:
        presentations = Presentation.objects.filter(
            conference_id=conference_id
        )
        return JsonResponse(
            {"presentations": presentations},
            PresentationListEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_presentation(request, id):
    if request.method == "DELETE":
        count, _ = Presentation.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})

    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "conference" in content:
                conference = Conference.objects.get(id=content["conference"])
                content["conference"] = conference
        except Conference.DoesNotExist:
            return JsonResponse({"message": "Invalid Conference ID"})
        Presentation.objects.filter(id=id).update(**content)
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(content, PresentationDetailEncoder, safe=False)

    else:
        presentation = Presentation.objects.get(id=id)
        return JsonResponse(
            presentation, PresentationDetailEncoder, safe=False
        )


def send_presentation_data(presentation, s, queue_name):
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host="rabbitmq")
    )
    channel = connection.channel()
    channel.queue_declare(queue=queue_name)
    content = {
        "presenter_name": presentation.presenter_name,
        "presenter_email": presentation.presenter_email,
        "title": presentation.title,
        "conference_name": presentation.conference.name,
    }
    channel.basic_publish(
        exchange="", routing_key=queue_name, body=json.dumps(content)
    )
    print(f"[x] Sent {content['title']} {s}.")
    connection.close()


@require_http_methods(["PUT"])
def api_approve_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.approve_presentation()
    send_presentation_data(presentation, "approval", 'presentation_approvals')
    return JsonResponse(presentation, PresentationDetailEncoder, safe=False)


@require_http_methods(["PUT"])
def api_reject_presentation(request, id):
    presentation = Presentation.objects.get(id=id)
    presentation.reject_presentation()
    send_presentation_data(presentation, "rejection", 'presentation_rejections')
    return JsonResponse(presentation, PresentationDetailEncoder, safe=False)
