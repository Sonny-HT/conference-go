import json, pika, django, os, sys, time
import datetime
from pika.exceptions import AMQPConnectionError

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()
from attendees.models import AccountVO


def update_AccountVO_callback(ch, method, properties, body):
    print("Received Account Information")
    content = json.loads(body)
    first_name = content["first_name"]
    last_name = content["last_name"]
    email = content["email"]
    is_active = content["is_active"]
    updated_string = content["updated"]
    updated = datetime.datetime.fromisoformat(updated_string)
    if is_active:
        AccountVO.objects.update_or_create(
            email=email,
            first_name=first_name,
            last_name=last_name,
            is_active=is_active,
            updated=updated,
        )
    else:
        try:
            AccountVO.objects.get(email=email).delete()
        except AccountVO.DoesNotExist:
            print(f"User does not exist: {email}")


def main():
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host="rabbitmq")
    )
    channel = connection.channel()
    channel.exchange_declare(exchange="account_info", exchange_type="fanout")
    result = channel.queue_declare(queue="", exclusive=True)
    queue_name = result.method.queue
    channel.queue_bind(exchange="account_info", queue=queue_name)

    print(" [*] Waiting for logs. To exit press CTRL+C")

    channel.basic_consume(
        queue=queue_name,
        on_message_callback=update_AccountVO_callback,
        auto_ack=True,
    )

    channel.start_consuming()


while True:
    try:
        main()
    except AMQPConnectionError:
        print("Cannot connect to RabbitMQ")
        time.sleep(4)
